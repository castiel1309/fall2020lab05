/*Castiel Le
 * 1933080*/
package movies.importer;

import java.io.*;

public class ProcessingTest {

	public static void main(String[] args) throws IOException {
		String source0 = "C:\\Users\\Castiel\\Desktop\\Programming II\\fall2020lab05\\testinput";
		String output0 = "C:\\Users\\Castiel\\Desktop\\Programming II\\fall2020lab05\\testoutput1";
		LowercasesProcessor test = new LowercasesProcessor(source0, output0);
		test.execute();
		
		String source1 = "C:\\Users\\Castiel\\Desktop\\Programming II\\fall2020lab05\\testoutput1";
		String output1 = "C:\\Users\\Castiel\\Desktop\\Programming II\\fall2020lab05\\testoutput2";
		RemoveDuplicates test1 = new RemoveDuplicates(source1, output1);
		test1.execute();
	}
}

