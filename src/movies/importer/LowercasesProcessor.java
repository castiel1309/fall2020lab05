/*Castiel Le
 * 1933080*/
package movies.importer;

import java.util.*;

public class LowercasesProcessor extends Processor {
	
	public LowercasesProcessor(String source, String output) {
		super(source, output, true);
	}
	
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> asLower = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			asLower.add(input.get(i).toLowerCase());
		}
		return asLower;
	}
}